package io.gitlab.rahimakhter.kidscode.ui.game

import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import io.gitlab.rahimakhter.kidscode.model.Action
import io.gitlab.rahimakhter.kidscode.model.GameMap
import io.gitlab.rahimakhter.kidscode.model.MapSquare.*
import io.gitlab.rahimakhter.kidscode.viewmodel.base.GameViewModel

@Composable
fun GameView(
	onQuit: () -> Unit
) {
	val viewModel: GameViewModel = viewModel()
	val level by viewModel.level.collectAsState()
	val game by viewModel.game.collectAsState()
	val map by viewModel.map.collectAsState()
	val player by viewModel.player.collectAsState()
	val availableActions by viewModel.availableActions.collectAsState()
	val actionSlots by viewModel.actionSlots.collectAsState()
	val isRunning by viewModel.isRunning.collectAsState()

	GameContent(
		level = level,
		game = game,
		map = map,
		playerLocation = player,
		availableActions = availableActions,
		actionSlots = actionSlots,
		isRunning = isRunning,
		setSlot = viewModel::setSlot,
		onPause = viewModel::pause,
		onStart = viewModel::start,
		onQuit = {
			viewModel.quit()
			onQuit()
		}
	)
}

@Preview(
	widthDp = 700,
	heightDp = 400
)
@Composable
fun PreviewGameContent() {
	var actionSlots: List<Action?> by remember { mutableStateOf(listOf(null, null, null)) }

	Surface {
		GameContent(
			level = 0,
			game = 0,

			map = GameMap(
				listOf(
					listOf(START, PATH, PATH, BLANK),
					listOf(BLANK, BLANK, PATH, BLANK),
					listOf(BLANK, BLANK, PATH, BLANK),
					listOf(BLANK, BLANK, PATH, END),
				),
			),
			playerLocation = 0 to 0,

			isRunning = false,
			onPause = {},
			onStart = {},
			onQuit = {},

			availableActions = listOf(Action.DOWN, Action.UP, Action.LEFT, Action.RIGHT),
			actionSlots = actionSlots,
			setSlot = { index, action ->
				val newList = ArrayList(actionSlots)
				newList[index] = action
				actionSlots = newList
			}
		)
	}
}

@Composable
fun GameContent(
	level: Int,
	game: Int,

	map: GameMap,
	playerLocation: Pair<Int, Int>,

	availableActions: List<Action>,
	actionSlots: List<Action?>,
	setSlot: (index: Int, action: Action?) -> Unit,

	isRunning: Boolean,
	onPause: () -> Unit,
	onStart: () -> Unit,
	onQuit: () -> Unit,
) {
	Scaffold(
		topBar = {
			GameTopBar(level, game, onQuit, isRunning, onStart, onPause)
		},
		bottomBar = {
			GameBottomBar(
				availableActions,
				actionSlots,
				setSlot
			)
		},
	) {
		GameMapContent(
			map,
			playerLocation,
			it
		)
	}
}
