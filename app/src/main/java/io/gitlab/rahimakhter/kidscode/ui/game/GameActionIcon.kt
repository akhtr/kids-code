package io.gitlab.rahimakhter.kidscode.ui.game

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Build
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import io.gitlab.rahimakhter.kidscode.R
import io.gitlab.rahimakhter.kidscode.model.Action

@Preview
@Composable
fun PreviewGameActionIcon() {

}

@Composable
fun GameActionIcon(
	action: Action
) {
	when (action) {
		Action.LEFT ->
			Icon(Icons.Default.KeyboardArrowLeft, stringResource(R.string.left))

		Action.RIGHT ->
			Icon(Icons.Default.KeyboardArrowRight, stringResource(R.string.right))

		Action.UP ->
			Icon(Icons.Default.KeyboardArrowUp, stringResource(R.string.up))

		Action.DOWN ->
			Icon(Icons.Default.KeyboardArrowDown, stringResource(R.string.down))

		Action.REPEAT ->
			Icon(Icons.Default.Refresh, stringResource(R.string.repeat))

		Action.USE ->
			Icon(Icons.Default.Build, stringResource(R.string.use))
	}
}