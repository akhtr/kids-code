package io.gitlab.rahimakhter.kidscode.model

enum class Action {
	/**
	 * Move Left
	 */
	LEFT,

	/**
	 * Move Right
	 */
	RIGHT,

	/**
	 * Move Up
	 */
	UP,

	/**
	 * Move Down
	 */
	DOWN,

	/**
	 * Repeat until hits something
	 */
	REPEAT,

	/**
	 * Break whatever it is facing
	 */
	USE
}