package io.gitlab.rahimakhter.kidscode.ui.game

import android.content.ClipData
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.draganddrop.dragAndDropSource
import androidx.compose.foundation.draganddrop.dragAndDropTarget
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Build
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draganddrop.DragAndDropTarget
import androidx.compose.ui.draganddrop.DragAndDropTransferData
import androidx.compose.ui.draganddrop.toAndroidDragEvent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.gitlab.rahimakhter.kidscode.R
import io.gitlab.rahimakhter.kidscode.model.Action
import io.gitlab.rahimakhter.kidscode.model.ActionDnD

@Preview
@Composable
fun PreviewGameActionSource() {
	GameActionSource(
		Action.USE
	)
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun GameActionSource(action: Action) {
	val context = LocalContext.current
	Box(
		Modifier
			.padding(2.dp)
			.size(48.dp)
			.background(Color.Green, CircleShape)
			.dragAndDropSource {
				detectDragGestures { _, _ ->
					startTransfer(
						DragAndDropTransferData(
							ClipData.newPlainText(
								context.getString(R.string.action),
								action.name
							),
							ActionDnD.FromSource(action)
						)
					)
				}
			},
		contentAlignment = Alignment.Center
	) {
		GameActionIcon(action)
	}
}