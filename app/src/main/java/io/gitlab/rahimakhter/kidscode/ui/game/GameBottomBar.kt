package io.gitlab.rahimakhter.kidscode.ui.game

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.gitlab.rahimakhter.kidscode.model.Action


@Preview(
	widthDp = 500,
)
@Composable
fun PreviewGameBottomBar() {
	Surface(
		Modifier.fillMaxWidth()
	) {
		GameBottomBar(
			listOf(Action.LEFT, Action.RIGHT, Action.UP, Action.DOWN),
			actionSlots = listOf(null, null, null, null),
			setSlot = { index, action ->
			}
		)
	}
}

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun GameBottomBar(
	availableActions: List<Action>,
	actionSlots: List<Action?>,
	setSlot: (index: Int, action: Action?) -> Unit,
) {
	Row(
		Modifier
			.fillMaxWidth()
			.padding(4.dp),
		horizontalArrangement = Arrangement.SpaceBetween,
		verticalAlignment = Alignment.CenterVertically
	) {
		FlowRow(
			Modifier.fillMaxWidth(.45f),
			horizontalArrangement = Arrangement.spacedBy(4.dp)
		) {
			actionSlots.forEachIndexed { index, action ->
				GameActionSlot(
					index,
					action,
					setAction = { newAction ->
						setSlot(index, newAction)
					},
					wipeSlot = {
						setSlot(it, null)
					}
				)
			}
		}


		FlowRow(
			Modifier.fillMaxWidth(.45f),
			horizontalArrangement = Arrangement.spacedBy(4.dp)
		) {
			availableActions.forEach { action ->
				GameActionSource(
					action,
				)
			}
		}


		GameGarbageSlot {
			setSlot(it, null)
		}
	}
}