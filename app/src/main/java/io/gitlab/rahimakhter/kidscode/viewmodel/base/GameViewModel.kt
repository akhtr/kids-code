package io.gitlab.rahimakhter.kidscode.viewmodel.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import io.gitlab.rahimakhter.kidscode.model.Action
import io.gitlab.rahimakhter.kidscode.model.GameMap
import kotlinx.coroutines.flow.StateFlow

abstract class GameViewModel(application: Application) : AndroidViewModel(application) {
	abstract val level: StateFlow<Int>
	abstract val game: StateFlow<Int>

	abstract val map: StateFlow<GameMap>
	abstract val player: StateFlow<Pair<Int, Int>>

	abstract val availableActions: StateFlow<List<Action>>
	abstract val actionSlots: StateFlow<List<Action?>>

	abstract fun setSlot(index: Int, action: Action?)

	abstract val isRunning: StateFlow<Boolean>

	abstract fun pause()

	abstract fun start()

	abstract fun quit()
}