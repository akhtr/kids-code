package io.gitlab.rahimakhter.kidscode.ui.game

import android.content.ClipData
import androidx.compose.animation.core.FastOutLinearInEasing
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.animateValue
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.draganddrop.dragAndDropSource
import androidx.compose.foundation.draganddrop.dragAndDropTarget
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draganddrop.DragAndDropTarget
import androidx.compose.ui.draganddrop.DragAndDropTransferData
import androidx.compose.ui.draganddrop.toAndroidDragEvent
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.gitlab.rahimakhter.kidscode.R
import io.gitlab.rahimakhter.kidscode.model.Action
import io.gitlab.rahimakhter.kidscode.model.ActionDnD


@Preview
@Composable
fun PreviewGameActionSlot() {
	Surface {
		GameActionSlot(0, action = Action.DOWN, setAction = {}, wipeSlot = {})
	}
}

val SLOT_SIZE = 48.dp
val SLOT_BIG_SIZE = 54.dp

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun GameActionSlot(
	index: Int,
	action: Action?,
	setAction: (Action) -> Unit,
	wipeSlot: (index: Int) -> Unit
) {
	val context = LocalContext.current
	var slotSize by remember { mutableStateOf(SLOT_SIZE) }
	var inSelectionMode by remember { mutableStateOf(false) }

	val rotation by shakeRotation()

	Box(
		Modifier
			.padding(4.dp)
			.let {
				if (inSelectionMode) {
					it.rotate(rotation)
				} else {
					it
				}
			}
			.background(Color.Gray)
			.size(slotSize)
			.dragAndDropTarget(
				shouldStartDragAndDrop = {
					it.toAndroidDragEvent().localState is ActionDnD
				},
				DragAndDropTarget(
					onDrop = {
						val dnd = (it.toAndroidDragEvent().localState as ActionDnD)
						setAction(dnd.action)
						if (dnd is ActionDnD.FromSlot) {
							wipeSlot(dnd.slot)
						}
						true
					},
					onStarted = {
						if ((it.toAndroidDragEvent().localState as? ActionDnD.FromSlot)?.slot != index) {
							inSelectionMode = true
						}
					},
					onEntered = {
						if ((it.toAndroidDragEvent().localState as? ActionDnD.FromSlot)?.slot != index) {
							slotSize = SLOT_BIG_SIZE
						}
					},
					onExited = {
						slotSize = SLOT_SIZE
					},
					onEnded = {
						inSelectionMode = false
						slotSize = SLOT_SIZE
					}
				)
			)
			.dragAndDropSource {
				if (action != null)
					detectDragGestures { _, _ ->
						startTransfer(
							DragAndDropTransferData(
								ClipData.newPlainText(
									context.getString(R.string.action),
									action.name
								),
								ActionDnD.FromSlot(action, index)
							)
						)
					}
			},
		contentAlignment = Alignment.Center
	) {
		if (action != null)
			GameActionIcon(action)
	}
}