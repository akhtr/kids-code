package io.gitlab.rahimakhter.kidscode.ui.game

import androidx.compose.animation.core.InfiniteTransition
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.draganddrop.dragAndDropTarget
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draganddrop.DragAndDropTarget
import androidx.compose.ui.draganddrop.toAndroidDragEvent
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.gitlab.rahimakhter.kidscode.R
import io.gitlab.rahimakhter.kidscode.model.ActionDnD

@Preview
@Composable
fun PreviewGameGarbageSlot() {
	Surface {
		GameGarbageSlot(wipeSlot = {})
	}
}

val GARBAGE_SIZE = 48.dp
val GARBAGE_BIG_SIZE = 54.dp

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun GameGarbageSlot(
	wipeSlot: (index: Int) -> Unit
) {
	var garbageSize by remember { mutableStateOf(GARBAGE_SIZE) }

	var inSelectionMode by remember { mutableStateOf(false) }
	val rotation by shakeRotation()

	Box(
		Modifier
			.padding(8.dp)
			.let {
				if (inSelectionMode) {
					it.rotate(rotation)
				} else {
					it
				}
			}
			.dragAndDropTarget(
				shouldStartDragAndDrop = {
					it.toAndroidDragEvent().localState is ActionDnD.FromSlot
				},
				DragAndDropTarget(
					onDrop = {
						wipeSlot((it.toAndroidDragEvent().localState as ActionDnD.FromSlot).slot)
						true
					},
					onStarted = {
						if (it.toAndroidDragEvent().localState is ActionDnD.FromSlot) {
							inSelectionMode = true
						}
					},
					onEntered = {
						if (it.toAndroidDragEvent().localState is ActionDnD.FromSlot) {
							garbageSize = GARBAGE_BIG_SIZE
						}
					},
					onExited = {
						if (it.toAndroidDragEvent().localState is ActionDnD.FromSlot) {
							garbageSize = GARBAGE_SIZE
						}
					},
					onEnded = {
						if (it.toAndroidDragEvent().localState is ActionDnD.FromSlot) {
							inSelectionMode = false
							garbageSize = GARBAGE_SIZE
						}
					}
				)
			),
		contentAlignment = Alignment.Center
	) {
		Icon(Icons.Default.Delete, stringResource(R.string.delete), Modifier.size(garbageSize))
	}
}
