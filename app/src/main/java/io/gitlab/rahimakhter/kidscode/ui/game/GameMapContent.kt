package io.gitlab.rahimakhter.kidscode.ui.game

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Face
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.gitlab.rahimakhter.kidscode.R
import io.gitlab.rahimakhter.kidscode.model.GameMap
import io.gitlab.rahimakhter.kidscode.model.MapSquare.*

@Preview
@Composable
fun PreviewGameMapContent() {
	Surface {
		GameMapContent(
			GameMap(
				listOf(
					listOf(START, PATH, PATH, BLANK),
					listOf(BLANK, BLANK, PATH, BLANK),
					listOf(BLANK, BLANK, PATH, BLANK),
					listOf(BLANK, BLANK, PATH, END),
				),
			),
			playerLocation = 2 to 0,
			PaddingValues(16.dp)
		)
	}
}

@Composable
fun GameMapContent(
	map: GameMap,
	playerLocation: Pair<Int, Int>,
	paddingValues: PaddingValues
) {
	Column(
		Modifier
			.padding(paddingValues)
			.fillMaxSize(),
		horizontalAlignment = Alignment.CenterHorizontally,
		verticalArrangement = Arrangement.Center
	) {
		map.map.forEachIndexed { rowNumber, columns ->
			Row {
				columns.forEachIndexed { columnNumber, field ->
					if (field is SPOT) {
						Card(
							Modifier
								.size(48.dp),
							colors = when (field) {
								is START -> {
									CardDefaults.cardColors(containerColor = Color.Green)
								}

								is END -> {
									CardDefaults.cardColors(containerColor = Color.Red)
								}

								else -> {
									CardDefaults.cardColors()
								}
							}
						) {
							AnimatedVisibility(
								rowNumber == playerLocation.first && columnNumber == playerLocation.second
							) {
								Box(
									Modifier.fillMaxSize(),
									contentAlignment = Alignment.Center,
								) {
									Icon(Icons.Default.Face, stringResource(R.string.me))
								}
							}
						}
					} else {
						Box(Modifier.size(48.dp))
					}
				}
			}
		}
	}
}