package io.gitlab.rahimakhter.kidscode.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
	@PrimaryKey(autoGenerate = true)
	val id: Int = 0,
	val name: String,
	val dateOfBirth: Long,
	val eMail: String,
	val passwordHash: String
)
