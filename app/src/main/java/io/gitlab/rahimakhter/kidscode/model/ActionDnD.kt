package io.gitlab.rahimakhter.kidscode.model

sealed interface ActionDnD {
	val action: Action

	data class FromSlot(
		override val action: Action,
		val slot: Int,
	) : ActionDnD

	data class FromSource(
		override val action: Action
	) : ActionDnD
}
