package io.gitlab.rahimakhter.kidscode.ui.game

import androidx.compose.animation.core.InfiniteTransition
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State


@Composable
fun shakeRotation(
	infiniteTransition: InfiniteTransition = rememberInfiniteTransition(label = "Shake")
): State<Float> =
	infiniteTransition.animateFloat(
		initialValue = -15f,
		targetValue = 15f,
		animationSpec = infiniteRepeatable(
			animation = tween(500, easing = LinearEasing),
			repeatMode = RepeatMode.Reverse
		),
		label = "Shake"
	)