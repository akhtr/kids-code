package io.gitlab.rahimakhter.kidscode.model

sealed interface MapSquare {
	data object BLANK : MapSquare
	interface SPOT : MapSquare
	data object START : SPOT
	data object END : SPOT
	data object PATH : SPOT
}