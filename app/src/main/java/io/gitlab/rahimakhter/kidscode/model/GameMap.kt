package io.gitlab.rahimakhter.kidscode.model

data class GameMap(
	val map: List<List<MapSquare>>
)