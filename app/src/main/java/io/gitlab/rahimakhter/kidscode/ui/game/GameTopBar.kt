package io.gitlab.rahimakhter.kidscode.ui.game

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.gitlab.rahimakhter.kidscode.R

@Preview
@Composable
fun PreviewGameTopBar() {
	Surface {
		GameTopBar(
			0,
			1,
			onQuit = {},

			isRunning = false,
			onStart = {},
			onPause = {}
		)
	}
}

@Composable
fun GameTopBar(
	level: Int,
	game: Int,
	onQuit: () -> Unit,

	isRunning: Boolean,
	onStart: () -> Unit,
	onPause: () -> Unit
) {
	Row(
		Modifier.fillMaxWidth(),
		verticalAlignment = Alignment.CenterVertically,
		horizontalArrangement = Arrangement.SpaceBetween
	) {
		Row(
			verticalAlignment = Alignment.CenterVertically,
			horizontalArrangement = Arrangement.spacedBy(4.dp)
		) {
			IconButton(onQuit) {
				Icon(Icons.Default.Close, stringResource(R.string.quit))
			}

			for (index in 0 until 3) {
				Box(
					Modifier
						.size(48.dp)
						.padding(4.dp)
						.background(
							if (level == 0) {
								Color.Gray
							} else {
								Color.LightGray
							}
						),
					contentAlignment = Alignment.Center
				) {
					if (index == game) {
						Icon(
							Icons.Default.Face,
							stringResource(R.string.me),
							tint = Color.Green
						)
					}
				}
			}
		}

		Row(
			verticalAlignment = Alignment.CenterVertically,
			horizontalArrangement = Arrangement.spacedBy(4.dp)
		) {
			Icon(Icons.Default.Refresh, stringResource(R.string.running))

			IconButton(onStart) {
				Icon(Icons.Default.PlayArrow, stringResource(R.string.start))
			}

			IconButton(onPause) {
				Icon(Icons.Default.Warning, stringResource(R.string.pause))
			}
		}
	}
}